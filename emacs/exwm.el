(require 'exwm)

;; Configurar el número de espacios de trabajo
(setq exwm-workspace-number 5)

;; Renombrar el buffer con el nombre de la clase de la ventana
(add-hook 'exwm-update-class-hook
          (lambda ()
            (exwm-workspace-rename-buffer exwm-class-name)))

;; Teclas globales
(setq exwm-input-global-keys
      `(([?\s-r] . exwm-reset) ;; s-r: Reset (modo línea).
        ([?\s-w] . exwm-workspace-switch) ;; s-w: Cambiar de espacio de trabajo.
        ([?\s-&] . (lambda (cmd) ;; s-&: Lanzar aplicaciones.
                     (interactive (list (read-shell-command "$ ")))
                     (start-process-shell-command cmd nil cmd)))
        ;; s-N: Cambiar a un espacio de trabajo específico.
        ,@(mapcar (lambda (i)
                    `(,(kbd (format "s-%d" i)) .
                      (lambda ()
                        (interactive)
                        (exwm-workspace-switch-create ,i))))
                  (number-sequence 0 9))))

;; Ajustar resolución con xrandr
(start-process-shell-command "xrandr" nil "xrandr --output HDMI-3 --mode 1366x768")

;; Forzar pantalla completa al iniciar
(add-hook 'emacs-startup-hook #'toggle-frame-fullscreen)

;; Habilitar EXWM
(exwm-enable)
