;;config
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-file "/home/jon/.emacs.d/packages.el")
(load-file "/home/jon/.emacs.d/fun.el")
(load-file "/home/jon/.emacs.d/exwm.el")

(setq inhibit-startup-message t)

;;inside window
(scroll-bar-mode -1)
(tool-bar-mode -1)    ;; disable the tool bar
(menu-bar-mode -1)    ;; disable the menu bar
(tooltip-mode -1)     ;; think it diable tips in below
(set-fringe-mode 10)  ;; dont know
(column-number-mode)
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)
;(global-hl-line-mode t)
(show-paren-mode t)
(global-display-line-numbers-mode t)
(setq x-select-enable-clipboard t)
(setq-default display-line-numbers-widen t)
(dolist (mode '(term-mode-hook
		shell-mode-hook
		pdf-view-mode-hook
		eshell-mode-hook
		vterm-mode-hook
		org-mode-hook))
  (add-hook mode(lambda() (display-line-numbers-mode 0))))

(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))

(add-hook 'org-mode-hook 'org-bullets-mode)

(add-hook 'vterm-mode-hook
          (lambda ()
            (local-set-key (kbd "C-M-b") 'counsel-switch-buffer)))

(pdf-tools-install)
(set-face-attribute 'default nil :height 100)

;;add clipboard to emacs terminal (emacs -nw)
;; Usar xclip para el portapapeles en modo terminal
(unless (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

(defun xclip-cut-function (text &optional push)
  (with-temp-buffer
    (insert text)
    (call-process-region (point-min) (point-max) "xclip" nil 0 nil "-i" "-selection" "clipboard")))

(defun xclip-paste-function()
  (let ((xclip-output (shell-command-to-string "xclip -o -selection clipboard")))
    (unless (string= (car kill-ring) xclip-output)
      xclip-output)))

;;for emacs -nw
(add-hook 'org-mode-hook 'org-indent-mode)
(setq interprogram-cut-function 'xclip-cut-function)
(setq interprogram-paste-function 'xclip-paste-function)

(add-hook 'org-mode-hook
          (lambda ()
            (define-key evil-normal-state-map (kbd "TAB") 'org-cycle)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;tranparency
(defun my-set-transparency ()
  (if (eq major-mode 'pdf-view-mode)
      (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
    (set-frame-parameter (selected-frame) 'alpha '(90 . 90))))

(add-hook 'pdf-view-mode-hook 'my-set-transparency)
(add-hook 'window-configuration-change-hook 'my-set-transparency)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;keybidndig
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key  (kbd "C-M-b") 'counsel-switch-buffer)
(global-set-key (kbd "C-c r") 'restart-emacs)

(define-key evil-normal-state-map (kbd "g f") nil)
(evil-define-key 'normal 'global (kbd "g f f") 'find-file-at-point)
(evil-define-key 'normal 'global (kbd "g f s") 'sudo-edit-find-file)
(evil-define-key 'normal 'global (kbd "g f l") 'load-file)

(evil-define-key 'normal 'global (kbd "g b f") 'counsel-switch-buffer)
(evil-define-key 'normal 'global (kbd "g b k ") 'kill-current-buffer)
(evil-define-key 'normal 'global (kbd "g b i") 'counsel-ibuffer)
(evil-define-key 'normal 'global (kbd "g b n") 'next-buffer)
(evil-define-key 'normal 'global (kbd "g b p") 'previous-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mode-line
;;clock on mode-line
(display-time-mode 1)
(setq display-time-format "%H:%M")

;;battery on mode-line
(display-battery-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;themes
;;(load-theme 'wombat t)
;;(load-theme 'doom-solarized-dark-high-contrast t)
;;(load-theme 'doom-palenight t)
;(load-theme 'doom-nord t)
;;(load-theme 'anti-zenburn t)
;;(load-theme 'modus-vivendi t)
(load-theme 'dracula t) 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;idk

(setq  backup-directory-alist            '((".*" . "~/.trash")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(exwm org-roam-ui org-evil evil-collection evil writeroom-mode which-key use-package tao-theme sudo-edit restart-emacs rainbow-delimiters pdf-tools org-roam org-bullets multi-vterm ivy-rich helpful greymatters-theme goto-chg general dracula-theme doom-themes doom-modeline daemons counsel command-log-mode cmake-mode brutalist-theme anti-zenburn-theme annalist all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

