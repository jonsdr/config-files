"""""""""""""""""""""""""""""
"       VIM CONFIG
"""""""""""""""""""""""""""""

"screen"

set nocompatible
set  number relativenumber

set laststatus=2
set mouse=a
set wrap
set cursorline

syntax enable

""pluggins""
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'arcticicestudio/nord-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'lervag/vimtex'
Plug 'lervag/vimtex', { 'tag': 'v2.15' }
call plug#end()

"system"
"let g:tempus_enforce_background_color=1
set expandtab
set tabstop=2
set shiftwidth=2


""theme""
colorscheme sex


""remaps""
noremap y "+y
noremap p "+p
noremap dd "+dd

""external configs
source ~/.vim/plugged/latex.vim

hi Normal guibg=NONE ctermbg=NONE
