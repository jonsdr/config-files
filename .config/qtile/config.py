

import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile import bar, layout, widget, extension, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
mod1 = "alt"
terminal = "st" 
home = os.path.expanduser('~')



languajes = [
    "setxkbmap -layout es",
    "setxkbmap -layout us"
]

def languaje(i):
    aux = languajes[i]
    os.system(aux)

keys = [

    ##moving though windows
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key(["mod1"], "Tab", lazy.layout.next(), desc="Move window focus to other window"),

    # Moving windows
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Growing windows
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),

    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),

    ##launch keys
    Key([mod], "e", lazy.spawn("emacsclient -c -e '(progn (split-window-below) (other-window 1) (enlarge-window -8) (multi-vterm)(treemacs) )' -a 'emacs -daemon'"), desc="launch emacs"),
    Key([mod], "Return", lazy.spawn("alacritty")), 
    Key([mod], "f", lazy.spawn("firefox")),
    Key([mod], "s", lazy.spawn("st spt")),
    Key([mod, "shift"], "Return", lazy.spawn("thunar")), 
 
    ##system keys 
    Key([mod], "Backspace", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
   #Key([mod, "control"], "q", lazy.spawn("loginctl terminate-user jon")),
    Key([mod, "control"], "q", lazy.spawn("dm-tool switch-to-greeter")), ##in case of sddm
    Key([mod], "Print", lazy.spawn("scrot /home/jon/Pictures/screenshots/'ArchLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' ")),
    Key([mod, "control"], "Return", lazy.function(languaje, 0)),
    Key([mod, "control"], "Return", lazy.function(languaje, 1)),
    Key([mod],"s", lazy.spawn("spotify-launcher")),
    Key([mod], "space", lazy.spawn("rofi -show drun")),
    
    ##Dmenu
    #Key([mod], 'r', lazy.run_extension(extension.DmenuRun(
    #    dmenu_prompt="$",
    #    dmenu_="Ubuntu Bold",
    #    background="#282c34",
    #    foreground="#c678dd",
    #    selected_background="#c678dd",
    #    selected_foreground=colors[2],
    #    dmenu_lines=10,
    #    fontsize=10,
    #))),

    ##volume
   # Key([mod1],"F1", lazy.spawn("amixer -D pulse set Master 1+ toggle")),
    Key(["mod1"],"F3", lazy.spawn("amixer set Master 5%+")),
    Key(["mod1"],"F2", lazy.spawn("amixer set Master 5%-")),
]


groups = [Group(i) for i in "1234567890"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

#doom - one theme 
colors = [["#282c34", "#282c34"],  #background = greyish
          ["#1c1f24", "#1c1f24"],  #
          ["#dfdfdf", "#dfdfdf"],  #foreground = blue 
          ["#ff6c6b", "#ff6c6b"],
          ["#98be65", "#98be65"],
          ["#da8548", "#da8548"],
          ["#51afef", "#51afef"],
          ["#c678dd", "#c678dd"],
          ["#46d9ff", "#46d9ff"],
          ["#a9a1e1", "#a9a1e1"],
          ["#CCCCCC","#CCCCCC"]]


colors3 = [
    [ "#333333"],  # grey (foreground)
    ["#242424"],  # black grey (background)
    ["#778899"],  # blue grey (line color)
    ["#fafafa"],  # white grey (font color)
    ["#ffffff"],  # white
    ["#000000"],  # black
    ["#a9a9a9"],  # grey (anti grey)
    ["#50394F"],  # tono 1 de colors3
    ["#593E5D"],  # tono 2 de colors3
    ["#000000"]   # tono 3 de colors3
]


theme = {"border_width": 3,
         "margin": 8,
         "border_focus": "e1acff",
         "border_focus_stack": "e1acff",
         "border_normal": "102330"}
theme2 = {
    "border_width": 2,
    "margin": 8,
    "border_focus": colors3[2],        # Púrpura menos brillante
    "border_focus_stack": colors3[2],  # Púrpura menos brillante
    "border_normal": "#0A1F29"         # Azul menos brillante
}

    
layouts = [
    layout.Max(**theme2),
    #layout.Columns(**theme), 
    layout.MonadTall(**theme2),
    #layout.Floating(**theme),
    #layout.MonadThreeCol(**theme),
   #layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]



widget_defaults = dict(
    font="san",
    fontsize=12,
    padding=3,
    background=colors3[1]
)
extension_defaults = widget_defaults.copy()

#prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

def init_widgets_list():
    widgets_list = [
        widget.TextBox(
            text = ' ',
            font = "Ubuntu Mono",
            background = colors3[1],
            foreground = colors3[1],
            padding = 2,
            fontsize = 12
        ),
        widget.GroupBox(
            font = "Ubuntu Bold",
            fontsize = 12,
            margin_y = 3,
            margin_x = 0,
            padding_y = 5,
            padding_x = 3,
            borderwidth = 3,
            active = colors3[2],
            inactive = colors3[0],
            rounded = False,
            highlight_color = colors3[1],
            highlight_method = "line",
            this_current_screen_border = colors3[2],
            foreground = colors3[0],
            background = colors3[1]
        ),
        widget.TextBox(
            text = '|',
            font = "Ubuntu Mono",
            background = colors3[1],
            foreground = colors3[2],
            padding = 2,
            fontsize = 12
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths = [os.path.expanduser("/home/jon/.config/qtile/icons")],
            foreground = colors3[2],
            background = colors3[1],
            padding = 0,
            scale = 0.7
        ),
        widget.CurrentLayout(
            foreground = colors3[2],
            background = colors3[1],
            fontsize = 12,
            padding = 5
        ),
        widget.TextBox(
            text = '|',
            font = "Ubuntu Mono",
            background = colors3[1],
            foreground = colors3[0],
            padding = 2,
            fontsize = 12
        ),
        widget.Prompt(),
        widget.WindowName(
            foreground = colors3[2],
            background = colors3[1],
            fontsize = 12,
            padding = 0
        ),
        widget.Chord(
            chords_colors={
                "launch": ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        ),
       #widget.Battery(
       #    font="Noto Sans",
       #    charge_char="+",
       #    format="{percent:2.0%}",
       #    update_interval = 10,
       #    fontsize = 12,
       #    foreground = colors3[2],
       #    background = colors3[1],
       #),
        widget.TextBox(
            text = '|',
            font = "Ubuntu Mono",
            background = colors3[1],
            foreground = colors3[2],
            padding = 2,
            fontsize = 12
        ),
        widget.Clock(
            foreground = colors3[2],
            background = colors3[1],
            fontsize = 12,
            format = "%A, %B %d - %H:%M "
        ),
        widget.TextBox(
            text = '|',
            font = "Ubuntu Mono",
            background = colors3[1],
            foreground = colors3[2],
            padding = 2,
            fontsize = 12
        ),
        widget.KeyboardLayout(
            configured_keybo = ['us', 'es'],
            foreground = colors3[1],
            background = colors3[1],
            fontsize = 12
        ),
        widget.Systray(
            background=colors3[1],
            foreground = colors3[1],
            icon_size=20,
            padding = 4
        ),
    ]
    return widgets_list
    

screens = [
    Screen(
        top=bar.Bar(
            init_widgets_list(),
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
    Click([mod], "Button2", lazy.window.toggle_floating()),  # Agregada línea para cambiar entre flotante y no flotante
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

##hooks
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/scripts/autostart.sh'])



# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"






