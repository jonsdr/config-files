;;config
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-file "/home/jon/.emacs.d/packages.el")
(load-file "/home/jon/.emacs.d/fun.el")
(load-file "/home/jon/.emacs.d/exwm.el")
;(load-file "/home/jon/.emacs.d/keys.el")

(setq inhibit-startup-message t)

;;inside window
(scroll-bar-mode -1)
(tool-bar-mode -1)    ;; disable the tool bar
(menu-bar-mode -1)    ;; disable the menu bar
(tooltip-mode -1)     ;; think it diable tips in below
(set-fringe-mode 10)  ;; dont know
(column-number-mode)
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)
;(global-hl-line-mode t)
(show-paren-mode t)
(global-display-line-numbers-mode t)
(setq x-select-enable-clipboard t)
;;function for hide ln from buffers
(setq-default display-line-numbers-widen t)
(dolist (mode '(term-mode-hook
		shell-mode-hook
		pdf-view-mode-hook
		eshell-mode-hook
		vterm-mode-hook
	;;	org-mode-hook
		))
  (add-hook mode(lambda() (display-line-numbers-mode 0))))

(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))

(add-hook 'org-mode-hook 'org-bullets-mode)

(add-hook 'vterm-mode-hook
          (lambda ()
            (local-set-key (kbd "C-M-b") 'counsel-switch-buffer)))

(pdf-tools-install)
(set-face-attribute 'default nil :height 100)

;;add clipboard to emacs terminal (emacs -nw)
;; Usar xclip para el portapapeles en modo terminal
(unless (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

(defun xclip-cut-function (text &optional push)
  (with-temp-buffer
    (insert text)
    (call-process-region (point-min) (point-max) "xclip" nil 0 nil "-i" "-selection" "clipboard")))

(defun xclip-paste-function()
  (let ((xclip-output (shell-command-to-string "xclip -o -selection clipboard")))
    (unless (string= (car kill-ring) xclip-output)
      xclip-output)))

;;for emacs -nw
(add-hook 'org-mode-hook 'org-indent-mode)
(setq interprogram-cut-function 'xclip-cut-function)
(setq interprogram-paste-function 'xclip-paste-function)

(add-hook 'org-mode-hook
          (lambda ()
            (define-key evil-normal-state-map (kbd "TAB") 'org-cycle)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;tranparency
(defun my-set-transparency ()
  (if (eq major-mode 'pdf-view-mode)
      (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
    (set-frame-parameter (selected-frame) 'alpha '(90 . 90))))

(add-hook 'pdf-view-mode-hook 'my-set-transparency)
(add-hook 'window-configuration-change-hook 'my-set-transparency)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;keybidndig
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key  (kbd "C-M-b") 'counsel-switch-buffer)
(global-set-key (kbd "C-c r") 'restart-emacs)

(define-key evil-normal-state-map (kbd "g f") nil)
(evil-define-key 'normal 'global (kbd "g f f") 'find-file-at-point)
(evil-define-key 'normal 'global (kbd "g f s") 'sudo-edit-find-file)
(evil-define-key 'normal 'global (kbd "g f l") 'load-file)

(evil-define-key 'normal 'global (kbd "g b f") 'counsel-switch-buffer)
(evil-define-key 'normal 'global (kbd "g b k ") 'kill-current-buffer)
(evil-define-key 'normal 'global (kbd "g b i") 'counsel-ibuffer)
(evil-define-key 'normal 'global (kbd "g b n") 'next-buffer)
(evil-define-key 'normal 'global (kbd "g b p") 'previous-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mode-line
;;clock on mode-line
(display-time-mode 1)
(setq display-time-format "%Y-%m-%d %H:%M")

;;battery on mode-line
(display-battery-mode 1)


(add-hook 'org-mode-hook 'org-fragtog-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;themes
;;(load-theme 'wombat t)
;;(load-theme 'doom-solarized-dark-high-contrast t)
;;(load-theme 'doom-palenight t)
;;(load-theme 'doom-nord t)
;;(load-theme 'anti-zenburn t)
;;(load-theme 'modus-vivendi t)
;;(load-theme 'dracula t)
;;(load-theme 'nord t)
;;load theme function

(if (daemonp)
    (add-hook 'after-make-frame-functions
        (lambda (frame)
            (with-selected-frame frame
                (load-theme 'doom-nord t))))
    (load-theme 'doom-nord t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Open at start.
(find-file "~/my-second-brain/bullet-journal/indexes/2025/index-2025-01.org")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;idk

(setq  backup-directory-alist            '((".*" . "~/.trash")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("571661a9d205cb32dfed5566019ad54f5bb3415d2d88f7ea1d00c7c794e70a36" "7c28419e963b04bf7ad14f3d8f6655c078de75e4944843ef9522dbecfcd8717d" "98b4ef49c451350c28a8c20c35c4d2def5d0b8e5abbc962da498c423598a1cdd" "603a831e0f2e466480cdc633ba37a0b1ae3c3e9a4e90183833bc4def3421a961" "014cb63097fc7dbda3edf53eb09802237961cbb4c9e9abd705f23b86511b0a69" "8c7e832be864674c220f9a9361c851917a93f921fedb7717b1b5ece47690c098" "456697e914823ee45365b843c89fbc79191fdbaff471b29aad9dcbe0ee1d5641" "ffafb0e9f63935183713b204c11d22225008559fa62133a69848835f4f4a758c" "88f7ee5594021c60a4a6a1c275614103de8c1435d6d08cc58882f920e0cec65e" "34cf3305b35e3a8132a0b1bdf2c67623bc2cb05b125f8d7d26bd51fd16d547ec" default))
 '(package-selected-packages
   '(nord-theme org-fragtog fragtog exwm-firefox-evil exwm org-roam-ui org-evil evil-collection evil writeroom-mode which-key use-package tao-theme sudo-edit restart-emacs rainbow-delimiters pdf-tools org-roam org-bullets multi-vterm ivy-rich helpful greymatters-theme goto-chg general dracula-theme doom-themes doom-modeline daemons counsel command-log-mode cmake-mode brutalist-theme anti-zenburn-theme annalist all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

