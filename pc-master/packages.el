;;; packages:
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package restart-emacs)

(use-package daemons)

(use-package sudo-edit)

(use-package command-log-mode)

(use-package swiper
  :ensure t)
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))


;;(use-package doom-modeline
;;  :ensure t
;;  :init (doom-modeline-mode 1))
;;(setq doom-modeline-icon nil)
;;(setq doom-modeline-icon nil) ; Deshabilita el icono del modo
;;(setq doom-modeline-major-mode-icon nil) ; Deshabilita el icono del modo mayor
;;(setq doom-modeline-minor-modes nil)
(use-package doom-modeline
  :ensure t
  :init
  (doom-modeline-mode 1)
  :config
  (setq doom-modeline-icon t) ; Deshabilita el icono del modo
  (setq doom-modeline-major-mode-icon t) ; Deshabilita el icono del modo mayor
  (setq doom-modeline-minor-modes nil)) ; Deshabilita la lista de modos menores

;;theming
(use-package nerd-icons) 

(use-package all-the-icons)

(use-package dracula-theme)

(use-package nord-theme)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t)) ; if nil, italics is universally disabled

(use-package rainbow-delimiters
  :hook (program-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("<menu>" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

 (use-package general
  :config
  ;;re configuraciones.
  (general-define-key
   :states 'motion
   ":" 'evil-ex)

  (general-create-definer my-leader
    :prefix "SPC")

  ;;File
  (my-leader
    :keymaps 'normal
    "f f" 'find-file-at-point
    "f s" 'sudo-edit-find-file
    "f l" 'load-file)

  ;;Print in current buffer
  (my-leader
    :keymaps 'normal
    ;;latex
    "p t" 'print-time
    "p l t" 'insert-latex-template
    "p l i" 'insert-latex-itemize
    "p l e" 'insert-latex-enumerate
    "p l v" 'insert-latex-verbatim
    ;;c++
    "p c c" 'insert-code-doc-c
    "p c f d" 'insert-code-fun-doc-c
    "p c f v" 'insert-void-function-code-c
    ;;org
    "p o l" 'insert-org-latex-code
    "p o m" 'print-month-template
    )
 

  ;;org-notes
  (my-leader
    :keymaps 'normal
    "n v" 'org-open-at-point-global
    "n l" 'org-roam-buffer-toggle
    "n f" 'org-roam-node-find
    "n i" 'org-roam-node-insert
    "n d" 'org-roam-dailies-map
    "n l" 'org-roam-new-lecture-note
    "n b" 'org-roam-dailies-capture-today
    "n n" 'org-roam-new-note
    "n u" 'org-roam-ui-open)

  ;;org-mode
  (my-leader
  :keymaps 'normal
  "t t" 'org-insert-todo-task
  "t e" 'org-insert-todo-event
  "t s t" 'org-insert-todo-task-subheading
  "t s e" 'org-insert-todo-event-subheading
  "t RET" 'org-fold-show-all
  "t DEL" 'org-fold-hide-sublevels
  "t s e" 'org-insert-todo-event-subheading
  "t d" 'org-todo)

  ;;Buffer managment
 (my-leader
 :keymaps 'normal
 "b f" 'counsel-switch-buffer
 "b k" 'kill-current-buffer
 "b i" 'counsel-ibuffer
 "b n" 'next-buffer
 "b p" 'previous-buffer)

 ;;open things 
 (my-leader
 :keymaps 'normal
 "RET" 'multi-vterm)
  )

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil ) ;; Establecer en t para que Evil Mode gestione las asignaciones de teclas
  :config
  (evil-mode 1))


(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))


(use-package vterm
  :ensure t)

(use-package org
  :bind (:map org-mode-map
              ("TAB" . my-org-toggle-subtree)))
              
(use-package writeroom-mode)

(use-package org-bullets)

(use-package pdf-tools)

(use-package project)

(use-package multi-vterm :ensure t)

(use-package command-log-mode)
(global-command-log-mode 1)

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/my-second-brain")
  (org-roam-dailies-directory "bullet-journal/bullets")
  (org-roam-completion-everywhere t)
  (org-roam-dailies-capture-templates
    '(("d" "default" entry "\n\n* %<%I:%M %p>:\n%?"
       :if-new (file+head "bullet-%<%Y-%m-%d>.org" "#+title: Bullet %<%Y-%m-%d>\n"))))
  :bind
  (("C-c n l" . org-roam-buffer-toggle)
   ("C-c n f" . org-roam-node-find)
   ("C-c n i" . org-roam-node-insert)
   :map org-mode-map
   ("C-M-i" . completion-at-point)
   :map org-roam-dailies-map
   ("Y" . org-roam-dailies-capture-yesterday)
   ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies) ;; Ensure the keymap is available
  (org-roam-db-autosync-mode))


(use-package format-all)

(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))
(setq-default flycheck-disabled-checkers '(tex-chktex))

(use-package auctex)

(use-package treemacs
  :config
  (treemacs-git-mode 'extended))
(use-package treemacs-evil
  :config
  (progn
    treemacs-width 30))  
(add-hook 'treemacs-mode-hook
          (lambda () (display-line-numbers-mode 0)))

(use-package org-evil)


(use-package exwm-firefox-evil)

(use-package exwm-firefox-core)

(use-package org-fragtog
  :config
  (add-hook 'org-mode-hook 'org-fragtog-mode))
