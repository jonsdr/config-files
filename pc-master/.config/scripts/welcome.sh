#!/bin/bash

# Función para centrar texto en la terminal
center_text() {
    local text="$1"
    local width=$(tput cols)
    local padding=$((($width + ${#text}) / 2))
    printf "%*s\n" $padding "$text"
}

# Mostrar mensaje de bienvenida centrado en la mitad de la pantalla
echo
center_text "

     ██  ██████  ███    ██ ███████ ██████  ██████  
     ██ ██    ██ ████   ██ ██      ██   ██ ██   ██ 
     ██ ██    ██ ██ ██  ██ ███████ ██   ██ ██████  
██   ██ ██    ██ ██  ██ ██      ██ ██   ██ ██   ██ 
 █████   ██████  ██   ████ ███████ ██████  ██   ██ 
                                                   
 "

echo
