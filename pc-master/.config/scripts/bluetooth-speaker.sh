
#!/bin/bash

# Función para verificar el estado del servicio bluetooth.service
check_bluetooth_service() {
    systemctl is-active --quiet bluetooth.service
    return $?
}

# Función para iniciar el servicio bluetooth.service
start_bluetooth_service() {
    sudo systemctl start bluetooth.service
}

# Función para obtener la dirección MAC del dispositivo bluetooth conectado
get_connected_device() {
    bluetoothctl info | grep "Device" | awk '{print $2}'
}

# Función para desconectar el dispositivo bluetooth actual
disconnect_device() {
    local current_device=$1
    bluetoothctl disconnect "$current_device"
}

# Función para conectar a la dirección MAC especificada
connect_device() {
    local target_device=$1
    bluetoothctl connect "$target_device"
}

sleep 5
# Dirección MAC del dispositivo objetivo
TARGET_DEVICE="38:8B:59:11:72:3A"

# Verificar el estado del servicio bluetooth.service
if ! check_bluetooth_service; then
    echo "El servicio bluetooth.service no está corriendo. Iniciando el servicio..."
    start_bluetooth_service
    sleep 2  # Esperar unos segundos para asegurarse de que el servicio se inicie correctamente
else
    echo "El servicio bluetooth.service está corriendo."
fi

# Obtener la dirección MAC del dispositivo bluetooth conectado
CONNECTED_DEVICE=$(get_connected_device)

if [ -z "$CONNECTED_DEVICE" ]; then
    echo "No hay ningún dispositivo conectado. Conectando a $TARGET_DEVICE..."
    connect_device "$TARGET_DEVICE"
elif [ "$CONNECTED_DEVICE" != "$TARGET_DEVICE" ]; then
    echo "Conectado a otro dispositivo ($CONNECTED_DEVICE). Desconectando y conectando a $TARGET_DEVICE..."
    disconnect_device "$CONNECTED_DEVICE"
    connect_device "$TARGET_DEVICE"
else
    echo "Ya está conectado al dispositivo objetivo ($TARGET_DEVICE)."
fi
