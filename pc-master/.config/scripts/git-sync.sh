#!/bin/bash

# Lista de directorios con repositorios Git
repos=(~/upiita ~/my-second-brain ~/config-files)

# Configuración inicial para evitar ingresar usuario y contraseña repetidamente
echo "Configurando credenciales para Git..."
read -p "Ingrese su usuario de Git: " git_user
read -sp "Ingrese su contraseña de Git: " git_password
echo

# Configurar helper de credenciales temporalmente para esta sesión
git config --global credential.helper store
echo "https://$git_user:$git_password@github.com" > ~/.git-credentials

# Iterar sobre los repositorios y realizar las acciones
for repo in "${repos[@]}"; do
    echo "Navegando al repositorio: $repo"
    cd "$repo" || { echo "No se pudo acceder a $repo"; continue; }

    # Verificar si hay cambios
    if [[ -n $(git status --porcelain) ]]; then
        echo "Cambios detectados en $repo. Realizando commit y push..."
        git add .
        git commit -m "Actualización automática: $(date)"
        git push
    else
        echo "No hay cambios en $repo."
    fi
done

# Limpieza de credenciales (opcional, elimina la credencial almacenada)
echo "Limpieza de credenciales..."
rm -f ~/.git-credentials
git config --global --unset credential.helper

echo "Proceso completado."
