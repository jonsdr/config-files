#
# ~/.bashrc


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

eval "$(starship init bash)"

#neofetch
#colorscript random

###########
##aliases##
###########

#bash completion
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

#Direcories
alias sudo='sudo '
alias ll='ls -la --color=auto'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias ..='cd ..'
alias ...='cd /'
#alias cl='clear && colorscript random'
alias cl='clear && date && cal'
alias src='source'
alias py='python3 '

PS1='[\u@\h \W]\$ '

#Text Editor
alias emg='emacsclient -c -a emacs'
alias emt='emacsclient -c -a emacs -nw' 
#alias vim='nvim '
alias scad='openscad'

#System commands
alias es='setxkbmap es'
alias us='setxkbmap us'
alias logout='loginctl terminate-user jon'
alias sy-ctl='sudo systemctl'
alias bye='shutdown now'
alias wait='systemctl suspend && exit'
alias gitlab='git config  user.email "jon_sdr@protonmail.com" && git config user.name "jon_sdr" ' 



#spotify

#WakeOnLan
##same lan
alias wake-master-here="wol d8:5e:d3:52:ab:bf" 

#source ~/.config/scripts/welcome.sh
cal && date 

