;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;personal functions 

;;pirint current time
(defun print-time ()
  (interactive)
  (with-current-buffer (current-buffer)
    (insert (format-time-string "[%H:%M]"))))
(global-set-key  (kbd "C-c p t") 'print-time)
;;print default Latex
(defun insert-latex-template ()
  "Insert a LaTeX template into the current buffer."
  (interactive)
  (insert "
\\documentclass[12pt,a4paper]{article}
\\usepackage[utf8]{inputenc}
\\usepackage{amsmath}
\\usepackage{amsfonts}
\\usepackage{amssymb}
\\usepackage{graphicx}
\\begin{document}

\\end{document}"))
(global-set-key (kbd "C-c p l t") 'insert-latex-template)

(defun insert-latex-itemize ()
  "Insert latex itemize code block."
  (interactive)
  (insert "
\\begin{itemize}
\\item
\\end{itemize}
"))
(global-set-key (kbd "C-c p l i") 'insert-latex-itemize)

(defun insert-latex-enumerate ()
  "Rnsert latex itemize code block."
  (interactive)
  (insert "
\\begin{enumerate}
\\item
\\end{enumerate}
"))
(global-set-key (kbd "C-c p l e") 'insert-latex-enumerate)

(defun insert-latex-verbatim ()
  "Insert latex itemize code block."
  (interactive)
  (insert "
\\begin{verbatim}

\\end{verbatim}
"))
(global-set-key (kbd "C-c p l v") 'insert-latex-verbatim)

(defun insert-code-doc-c ()
  "Insert default documentation to a code."
  (interactive)
  (insert "/**
 * @file practica
 * @author Jonathan Samuel Delgado Rodriguez (jonadero29@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include<iostream>
using namespace std;

int main(){


  return 0;
}
"))


(global-set-key (kbd "C-c p c c") 'insert-code-doc-c )

(defun insert-code-fun-doc-c ()
  "Insert default documentation to a function in c."
  (interactive)
  (insert "
/**
 * @brief 
 * 
 * @param
 * @param 
 */"))


(global-set-key (kbd "C-c p c f d") 'insert-code-fun-doc-c)

(defun insert-for-code-c ()
  "Insert default documentation to a code."
  (interactive)
  (insert "for(int i=0,i<n,i++){

}
"))

(global-set-key (kbd "C-c p c f f") 'insert-for-code-c)

(defun insert-void-function-code-c ()
  "Insert default documentation to a code."
  (interactive)
  (insert "void function(int x, int y){

}
"))

(global-set-key (kbd "C-c p c f v") 'insert-void-function-code-c)

(defun insert-function-code-c ()
  "Insert default documentation to a code."
  (interactive)
  (insert "int function(int x, int y){

  return 0;
}
"))
(global-set-key (kbd "C-c p c f i") 'insert-function-code-c )

;;open vterm(multi)
(defun terminal ()
  "Open a multi-vterm instance."
  (interactive)
  (multi-vterm))
(global-set-key (kbd "C-c v") 'terminal)

(global-set-key (kbd "C-c t s") 'org-timer-set-timer)
(global-set-key (kbd "C-c t RET") 'org-timer-pause-or-continue)
(global-set-key (kbd "C-c t DEL") 'org-timer-stop)
(setq org-clock-sound "~/.emacs.d/halo.wav")

;; principios de programacion
(defun insert-fundamentos-latex-template ()
  "Insert latex template."
  (interactive)
  (insert "
\\documentclass[10pt,a4paper]{article}
\\usepackage[utf8]{inputenc}
\\usepackage{amsmath}
\\usepackage{amsfonts}
\\usepackage{amssymb}
\\usepackage{graphicx}
\\usepackage{geometry}
\\geometry{margin=1in}
\\title{\\vspace{2in} \\Huge Análisis y Diseño de Programas \\\\ \large Práctica}
\\author{
Jonathan Samuel Delgado Rodríguez \\\\ 2021330263 \\\\ 
Ingeniería Mecatrónica \\\\ 1MV2
}
\\date{\\vspace{1in} \\today}
\\begin{document}
\\maketitle
\\newpage
\\section*{Objetivo}
\\section*{Introducción}
\\section*{Desarrollo}
\\section*{Ejecución}
\\section*{Conclusiones}
\\end{document}"))

(global-set-key (kbd "C-c p l c") 'insert-fundamentos-latex-template)


(defun generar-tablas-requerimientos ()
  "Generar tablas para los requerimientos del 3 al 30."
  (interactive)
  (dotimes (i 28)  ; 28 tablas desde el requerimiento 3 hasta el 30
    (let ((num (+ i 3)))  ; El número del requerimiento (comienza en 3)
      (insert (format "\\subsection{Requerimiento %d}\n" num))
      (insert "\\begin{tabular}{|p{4cm}|p{10cm}|}\n")
      (insert "    \\hline\n")
      (insert (format "    \\multicolumn{2}{|c|}{\\textbf{Requerimiento %d}} \\\\ \\hline\n" num))
      (insert "    \\textbf{Función} & \\\\ \\hline\n")
      (insert "    \\textbf{Descripción} & \\\\ \\hline\n")
      (insert "    \\textbf{Fuentes} & \\\\ \\hline\n")
      (insert "    \\textbf{Destino} & \\\\ \\hline\n")
      (insert "    \\textbf{Requerimiento} & \\\\ \\hline\n")
      (insert "    \\textbf{Precondición} & \\\\ \\hline\n")
      (insert "    \\textbf{Acción} & \\\\ \\hline\n")
      (insert "    \\textbf{Postcondición} & \\\\ \\hline\n")
      (insert "    \\textbf{Efectos colaterales} & \\\\ \\hline\n")
      (insert "\\end{tabular}\n\n"))))

(defun org-roam-new-lecture-note ()
  "Crea una nueva nota de clase en un directorio bajo /org-notes/roam-notes/.  Agrega automáticamente un ID de Org-roam.  Si el archivo ya existe, simplemente ábrelo."
  (interactive)
  (let* ((base-directory (expand-file-name "~/my-second-brain/"))
         (target-directory (read-directory-name "Selecciona o crea el directorio para la nota: " base-directory))
         (date (format-time-string "%Y-%m-%d"))
         (subject (file-name-base (directory-file-name target-directory))) ; Obtener el nombre del directorio como materia
         (filename (concat subject "-" date ".org"))
         (filepath (expand-file-name filename target-directory)))
    ;; Crear el directorio si no existe
    (unless (file-directory-p target-directory)
      (make-directory target-directory t))

    ;; Verificar si el archivo ya existe
    (if (file-exists-p filepath)
        (progn
          (message "El archivo ya existe: %s" filepath)
          (find-file filepath))
      ;; Crear el archivo de la nota
      (let ((id (org-id-new))) ; Generar un ID único para Org-roam
        (with-temp-file filepath
          (insert ":PROPERTIES:\n")
          (insert ":ID: " id "\n") ; Agregar ID de Org-roam
          (insert ":END:\n\n")
          (insert "#+title: " subject " " date "\n")
          (insert "#+date: " date "\n")
          (insert "#+filetags: :lecture: " (replace-regexp-in-string " " "-" (downcase subject)) "\n")
          (insert "\n* "date"\n** Resumen\n\n** Notas\n\n** To do's [0/0]\n")))
      ;; Confirmar al usuario y abrir el archivo
      (message "Nota creada: %s" filepath)
      (find-file filepath))))

(global-set-key (kbd "C-c n n") 'org-roam-new-lecture-note)

;;the next is for org-roam ui

(global-set-key (kbd "C-c n u") 'org-roam-ui-open)

(defun org-roam-new-note ()
  "Crea una nueva nota en un directorio especificado y permite elegir el nombre del archivo. Agrega automáticamente un ID de Org-roam."
  (interactive)
  (let* ((base-directory (expand-file-name "~/my-second-brain/"))
         (target-directory (read-directory-name "Selecciona o crea el directorio para la nota: " base-directory))
         (filename (read-string "Nombre del archivo (sin extensión): "))
         (filepath (expand-file-name (concat filename ".org") target-directory)))
    ;; Crear el directorio si no existe
    (unless (file-directory-p target-directory)
      (make-directory target-directory t))

    ;; Verificar si el archivo ya existe
    (if (file-exists-p filepath)
        (progn
          (message "El archivo ya existe: %s" filepath)
          (find-file filepath))
      ;; Crear el archivo de la nota
      (let ((id (org-id-new)) ; Generar un ID único para Org-roam
            (date (format-time-string "%Y-%m-%d")))
        (with-temp-file filepath
          (insert ":PROPERTIES:\n")
          (insert ":ID: " id "\n") ; Agregar ID de Org-roam
          (insert ":END:\n\n")
          (insert "#+title: " filename "\n")
          (insert "#+date: " date "\n")
          (insert "#+filetags: :note: " (replace-regexp-in-string " " "-" (downcase filename)) "\n")
          (insert "\n* Resumen\n\n* Notas\n\n* To do's\n")))
      ;; Confirmar al usuario y abrir el archivo
      (message "Nota creada: %s" filepath)
      (find-file filepath))))

(defun org-insert-todo-task ()
  "Inserta un encabezado TODO con el formato '* TODO [TASK]'."
  (interactive)
  (org-insert-heading) ;; Inserta un encabezado nuevo
  (org-todo 'todo) ;; Convierte el encabezado en un TODO
  (insert "[TASK] . [0/0]") ;; Inserta el texto '[TASK]'
  (end-of-line)) ;; Coloca el cursor al final de la línea

(defun org-insert-todo-event ()
  "Inserta un encabezado TODO con el formato '* TODO [TASK]'."
  (interactive)
  (org-insert-heading) ;; Inserta un encabezado nuevo
  (org-todo 'todo) ;; Convierte el encabezado en un TODO
  (insert "[EVENT]") ;; Inserta el texto '[TASK]'
  (end-of-line)) ;; Coloca el cursor al final de la línea

(defun org-insert-todo-task-subheading ()
  "Inserta un subencabezado TODO con el formato '** TODO [TASK]'."
  (interactive)
  (org-insert-subheading nil) ;; Inserta un subencabezado
  (org-todo 'todo) ;; Convierte el subencabezado en un TODO
  (insert "[TASK] . [0/0]") ;; Inserta el texto '[TASK]'
  (end-of-line)) ;; Coloca el cursor al final de la línea

(defun org-insert-todo-event-subheading ()
  "Inserta un subencabezado TODO con el formato '** TODO [TASK]'."
  (interactive)
  (org-insert-subheading nil) ;; Inserta un subencabezado
  (org-todo 'todo) ;; Convierte el subencabezado en un TODO
  (insert "[EVENT]") ;; Inserta el texto '[TASK]'
  (end-of-line)) ;; Coloca el cursor al final de la línea

(defun org-insert-current-time-heading ()
  "Inserta un encabezado en Org-mode con la hora actual en formato '* [I:M]'."
  (interactive)
  (let ((current-time (format-time-string "%H:%M"))) ;; Obtiene la hora actual en formato 24 horas
    (org-insert-heading) ;; Inserta un nuevo encabezado
    (insert (format "[%s]" current-time)) ;; Inserta la hora actual entre corchetes
    (end-of-line))) ;; Coloca el cursor al final de la línea


(require 'org) ;; Asegúrate de tener Org-mode cargado

(require 'org) ;; Asegúrate de tener Org-mode cargado

(require 'org) ;; Asegúrate de tener Org-mode cargado

(defun print-month-template (month)
  "Genera un índice mensual en Org-mode.
Pide el mes (en formato numérico) y usa el año actual.
Crea un template con un encabezado y las fechas del mes alineadas dinámicamente con 'Insert Bullet'."
  (interactive "nEnter month (1-12): ") ;; Solicita al usuario el mes
  (let* ((year (string-to-number (format-time-string "%Y"))) ;; Obtiene el año actual
         (days-in-month (calendar-last-day-of-month month year)) ;; Calcula el número de días del mes
         (month-name (calendar-month-name month)) ;; Obtiene el nombre del mes
         (template (format "This is %s index.

* To do's

* Bullets\n" month-name))
         (max-width 0)) ;; Variable para calcular la longitud máxima de las fechas
    ;; Calcula la longitud máxima de las fechas
    (dotimes (day days-in-month)
      (let* ((date (format "%02d %s %d" (1+ day) month-name year))
             (day-name (format-time-string "%A" (encode-time 0 0 0 (1+ day) month year)))
             (full-date (format "%s - %s" date day-name)))
        (setq max-width (max max-width (length full-date))))) ;; Actualiza el ancho máximo
    
    ;; Construye las líneas de bullets alineadas dinámicamente
    (dotimes (day days-in-month)
      (let* ((date (format "%02d %s %d" (1+ day) month-name year))
             (day-name (format-time-string "%A" (encode-time 0 0 0 (1+ day) month year)))
             (full-date (format "%s - %s" date day-name))
             (spaces (make-string (+ 7 (- max-width (length full-date))) ?\s))) ;; Calcula espacios dinámicamente
        (setq template (concat template (format "%s%sInsert Bullet\n" full-date spaces)))))
    
    ;; Inserta el resultado en el buffer actual
    (insert template)))

(defun insert-org-latex-code ()
  "Insert latex template."
  (interactive)
  (insert "
#+BEGIN_Latex

#+END_Latex
"))
