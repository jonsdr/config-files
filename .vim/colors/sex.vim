"sex colorxcheme
"
highlight clear

if exists("syntax_on")
    syntax reset
endif
let g:colors_name = "sex"

" Define a simplified palette
let s:palette = {}
let s:palette.white = [254, '#ffffff']

let s:palette.gray = [245, '#404040']
let s:palette.light_gray = [253, '#f0f0f0'] " Added light gray for cursor line
let s:palette.black = [233, '#000000']
let s:palette.red = [88, '#870000']
let s:palette.dark_gray = [240, '#303030'] " Added darker gray for comments
let s:palette.blue = [21, '#5f00af'] " More contrasting blue color

" Define a simplified function for highlights
function! s:hi(group, fg_color, bg_color, style)
    let highlight_command = ['hi', a:group]
    if !empty(a:fg_color)
        let [ctermfg, guifg] = a:fg_color
        call add(highlight_command, printf('ctermfg=%d guifg=%s', ctermfg, guifg))
    endif
    if !empty(a:bg_color)
        let [ctermbg, guibg] = a:bg_color
        call add(highlight_command, printf('ctermbg=%d guibg=%s', ctermbg, guibg))
    endif
    if !empty(a:style)
        call add(highlight_command, printf('cterm=%s gui=%s', a:style, a:style))
    endif
    execute join(highlight_command, ' ')
endfunction

" Apply highlights with red/blue palette and lighter cursor line
call s:hi('Normal', s:palette.black, s:palette.white, '')
call s:hi('Comment', s:palette.dark_gray, [], 'italic') " Darker gray for comments
call s:hi('Constant', s:palette.red, [], 'bold')
call s:hi('String', s:palette.blue, [], '') " Changed to more contrasting blue
call s:hi('Identifier', s:palette.blue, [], 'none') " Changed to more contrasting blue
call s:hi('Statement', s:palette.red, [], 'bold')
call s:hi('PreProc', s:palette.red, [], 'none')
call s:hi('Type', s:palette.blue, [], 'bold') " Changed to more contrasting blue
call s:hi('Special', s:palette.red, [], '')
call s:hi('LineNr', s:palette.gray, s:palette.white, 'none')
call s:hi('CursorLine', [], s:palette.light_gray, 'none') " Lighter color for cursor line
call s:hi('CursorLineNr', s:palette.black, s:palette.light_gray, 'none') " Adjusted to match cursor line
call s:hi('Visual', [], s:palette.gray, '')
call s:hi('Search', s:palette.white, s:palette.red, 'none')
call s:hi('Error', s:palette.red, s:palette.white, 'bold')
call s:hi('WarningMsg', s:palette.red, s:palette.white, '')

" Set background
set background=light
