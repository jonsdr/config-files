#!/bin/bash

function run {
    if ! pgrep -x $(basename $1 | head -c 15) 1>/dev/null;
    then
	$@&
    fi
}

############
##programs##
############
run emacs --daemon &
#run spotifyd  &
run kmix --keepvisibility &
run cbatticon &
run nm-applet &
blueberry-tray &
run picom &
run nitrogen --restore &

###########
##scripts##
###########
/home/jon/.config/scripts/usbs.sh &
